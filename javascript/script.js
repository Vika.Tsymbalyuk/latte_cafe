// const btnFoodMenu = $('button[ data-menu="menu_1"]')
// btnDrinksMwnu = $('button[ data-menu="menu_2"]')
// btnTakeawaydMenu = $('button[ data-menu="menu_3"]')
//
// btnFoodMenu.click(function (){
//     $('.data-menu[menu]').removeClass('open')
//     $('.menu[data-menu="menu_1"]').add('open')
//
// })
//
//
// btnDrinksMwnu.click(function (){
//     $('.data-menu[menu]').removeClass('open')
//     $('.menu[data-menu="menu_2"]').add('open')
//
// })
// btnTakeawaydMenu.click(function (){
//     $('.data-menu[menu]').removeClass('open')
//     $('.menu[data-menu="menu_3"]').add('open')
//
// })


const menuButtons = $('button[data-menu]')

menuButtons.click(function () {

    const menu = ($(this).data('menu'))

    $('.menu[data-menu]').removeClass('open')

    $(`.menu[data-menu="${menu}"]`).addClass('open')

    menuButtons.removeClass('btn-active')

    $(this).addClass('btn-active')

})


$(document).ready(function () {
    let owl = $('.owl-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        onInitialized: updateSlideNumbers,
        onChanged: updateSlideNumbers,
    });

    $('.go-to-first').on('click', function () {
        owl.trigger('to.owl.carousel', 0);
    });

    $('.go-to-last').on('click', function () {
        let lastSlideIndex = owl.find('.owl-item:last').index();
        owl.trigger('to.owl.carousel', lastSlideIndex);
    });

    function updateSlideNumbers(event) {
        let currentItem = event.item.index + 1;
        let totalItems = event.item.count;
        let nextItem = currentItem === totalItems ? 1 : currentItem + 1;
        let prevItem = currentItem === 1 ? totalItems : currentItem - 1;

        $('.current').text(currentItem);
        $('.next').text(nextItem);
        $('.prev').text(prevItem);
    }
});




